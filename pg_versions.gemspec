require_relative "lib/pg_versions/version"

Gem::Specification.new do |s|
  s.name        = "pg_versions"
  s.version     = PgVersions::VERSION
  s.authors     = ["yunta"]
  s.email       = ["maciej.blomberg@mikoton.com"]
  s.homepage    = "https://gitlab.com/yunta/pg-versions"
  s.summary     = "Persistent timestamped postgres notification library"
  s.description = "PostgreSQL notifications with persistence."
  s.license     = "MIT"

  s.files = Dir["{db,lib}/**/*", "MIT-LICENSE", "Rakefile", "create-table.sql", "drop-table.sql"]
  s.require_paths = ["lib"]

  s.add_development_dependency "rspec", "~>3.10"
  s.add_development_dependency "simplecov", "~>0.21"
  s.add_development_dependency "activerecord", "~>7.0"
  s.add_dependency "pg", "~> 1.2"
end
