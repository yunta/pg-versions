class CreatePgVersionsTable < ActiveRecord::Migration[7.0]

        def up
                PgVersions.create_table(ActiveRecord::Base)
        end

        def down
                PgVersions.drop_table(ActiveRecord::Base)
        end

end
