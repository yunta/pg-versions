require 'pg'
require 'pg_versions'
require 'active_record'

RSpec.describe PgVersions do

	before(:example) do
		$connection = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		$connection2 = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		PgVersions::drop_table($connection)
		PgVersions::create_table($connection)
	end


	it "dies verbosely if it doesn't know how to connect to the database" do
		Object.send(:remove_const, :ActiveRecord)
		pg_versions = PgVersions::Connection.new
		expect {
			pg_versions.process
		}.to raise_error(PgVersions::InvalidParameters)
	end

end
