require 'active_record'
require 'pg'
require 'pg_versions'


#TODO: this fails sometimes, why?
def kill_connection(killing_connection, connection_to_kill)
	$connection2.exec("SELECT pg_terminate_backend(#{connection_to_kill.backend_pid});") 
end

def change_password(changing_connection, new_password)
	$connection2.exec("ALTER USER test PASSWORD '#{new_password}';") 
end

RSpec.describe PgVersions do

	before(:example) do
		$connection = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		$connection2 = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		$connection3 = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		PgVersions::drop_table($connection)
		PgVersions::create_table($connection)
	end


	it "can bump, and read-back a version, after reconnection" do
		pg_versions = PgVersions::Connection.new
		processing_thread_1 = Thread.new { pg_versions.process($connection) }
		bumped = pg_versions.bump("xxx", "yyy")
		read = pg_versions.read("xxx", "yyy", "zzz")
		
		kill_connection($connection2,$connection)
		
		expect { processing_thread_1.join }.to raise_error(PG::ConnectionBad)
		processing_thread_2 = Thread.new { pg_versions.process($connection3) }
		expect(read.size).to eq 3
		expect(read["xxx"]).to eq bumped["xxx"]
		expect(read["yyy"]).to eq bumped["yyy"]
		read2 = pg_versions.read("zzz")
		expect(read2.size).to eq 1
		expect(read2["zzz"]).to eq read["zzz"]
		pg_versions.close
		processing_thread_2.join
	end

	
	it "keeps receiving notifications after reconnection" do
		pg_versions = PgVersions::Connection.new
		processing_thread_1a = Thread.new { pg_versions.process($connection) }
		pg_versions_2 = PgVersions::Connection.new
		processing_thread_2 = Thread.new { pg_versions_2.process($connection2) }
		subscription = pg_versions.subscribe("xxx")
		event = subscription.wait
		expect(event).not_to be_nil
		
		kill_connection($connection3,$connection)
		
		expect { processing_thread_1a.join }.to raise_error(PG::ConnectionBad)
		processing_thread_1b = Thread.new { pg_versions.process($connection3) }
		sleep 0.5
		wait = Thread.new { subscription.wait }
		expect(wait.alive?).to eq true
		bumped = pg_versions_2.bump("xxx")
		notification = wait.value
		expect(notification.changed).to eq({"xxx"=>bumped["xxx"]})
		pg_versions.close
		processing_thread_1b.join
		pg_versions_2.close
		processing_thread_2.join
	end


	it "receives versions bumped while disconnected, if wait starts after disconnection" do
		pg_versions = PgVersions::Connection.new
		processing_thread_1a = Thread.new { pg_versions.process($connection) }
		pg_versions_2 = PgVersions::Connection.new
		processing_thread_2 = Thread.new { pg_versions_2.process($connection2) }
		subscription = pg_versions.subscribe("xxx")
		event = subscription.wait
		expect(event).not_to be_nil
	
		kill_connection($connection3, $connection)

		expect { processing_thread_1a.join }.to raise_error(PG::ConnectionBad)
		sleep 0.5
		bumped = pg_versions_2.bump("xxx")
		processing_thread_1b = Thread.new { pg_versions.process($connection3) }
		sleep 0.5
		notification = subscription.wait
		expect(notification.changed).to eq({"xxx"=>bumped["xxx"]})
		pg_versions.close
		processing_thread_1b.join
		pg_versions_2.close
		processing_thread_2.join
	end


	it "keeps receiving notifications after reconnection, if wait starts before disconnection" do
		pg_versions = PgVersions::Connection.new
		processing_thread_1a = Thread.new { pg_versions.process($connection) }
		pg_versions_2 = PgVersions::Connection.new
		processing_thread_2 = Thread.new { pg_versions_2.process($connection2) }
		subscription = pg_versions.subscribe("xxx")
		event = subscription.wait
		expect(event).not_to be_nil

		wait = Thread.new { subscription.wait }

		kill_connection($connection3,$connection)

		expect { processing_thread_1a.join }.to raise_error(PG::ConnectionBad)
		sleep 0.5
		bumped = pg_versions_2.bump("xxx")
		expect(wait.alive?).to eq true
		processing_thread_1b = Thread.new { pg_versions.process($connection3) }
		notification = wait.value
		expect(notification.changed).to eq({"xxx"=>bumped["xxx"]})
		pg_versions.close
		processing_thread_1b.join
		pg_versions_2.close
		processing_thread_2.join
		wait.join
	end

end