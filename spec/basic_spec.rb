require 'active_record'
require 'pg'
require 'pg_versions'

using PgVersions

RSpec.describe PgVersions do

	before(:example) do
		$connection = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		$connection2 = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
		PgVersions::drop_table($connection)
		PgVersions::create_table($connection)
	end

	
	it "can bump, and read-back a version" do
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		bumped = pg_versions.bump("xxx", "yyy")
		expect(bumped.size).to eq 2
		expect(bumped["xxx"]).not_to be nil
		expect(bumped["yyy"]).not_to be nil
		read = pg_versions.read("xxx", "yyy", "zzz")
		expect(read.size).to eq 3
		expect(read["xxx"]).to eq bumped["xxx"]
		expect(read["yyy"]).to eq bumped["yyy"]
		expect(read["zzz"]).not_to be nil
		read2 = pg_versions.read("zzz")
		expect(read2.size).to eq 1
		expect(read2["zzz"]).to eq read["zzz"]
		pg_versions.close
		processing_thread.join
	end


	it "allows waiting for version bumps, without pre-known versions" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx")
		event = subscription.wait
		expect(event).not_to be_nil
		bumped = false
		t1 = Thread.new {
			sleep 0.5
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			thread2.pop
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})
		thread2 << :your_turn
		thread1.pop
		notification = subscription.wait
		expect(notification.changed_versions).to eq({'xxx'=>bumped['xxx']})
		pg_versions.close
		processing_thread.join
		t1.join
	end

	it "raises exception when trying to process when connection is already being processed" do
		pg_versions = PgVersions::Connection.new
		processing_thread_1 = Thread.new { pg_versions.process($connection) }
		processing_thread_2 = Thread.new { pg_versions.process($connection) }
		expect {
			processing_thread_2.join
		}.to raise_error(RuntimeError)
		pg_versions.close
		processing_thread_1.join
	end


	it "allows bumping, and selectively wait or not for notify coming from that bump" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx")
		bumped = subscription.bump(notify: false)
		expect(bumped.size).to eq 1
		notified = false
		t1 = Thread.new {
			sleep 0.2
			expect(notified).to eq false
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		notified = true
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})

		bumped = subscription.bump(notify: true)
		notification = subscription.wait
		expect(notification.changed_versions).to eq({'xxx'=>bumped['xxx']})
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "allows reading, and selectively wait or not for returned versions" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx")
		bumped = subscription.bump(notify: true)
		read = subscription.read(notify: false)
		expect(bumped.size).to eq 1
		expect(bumped).to eq read
		notified = false
		t1 = Thread.new {
			sleep 0.2
			expect(notified).to eq false
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		notified = true
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})

		bumped = subscription.bump(notify: true)
		read = subscription.read(notify: true)
		notification = subscription.wait
		expect(bumped.size).to eq 1
		expect(bumped).to eq read
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "allows waiting for version bumps, without pre-known versions" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx")
		event = subscription.wait
		expect(event).not_to be_nil
		bumped = false
		t1 = Thread.new {
			sleep 0.5
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			thread2.pop
			bumped = pg_versions_2.bump("xxx")
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})
		thread2 << :your_turn
		thread1.pop
		notification = subscription.wait
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "allows unsubscribing" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx","yyy")
		subscription.read(notify: false)  # clearing notify queue
		subscription.unsubscribe("xxx")
		notified = false
		bumped = nil
		t1 = Thread.new {
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped = pg_versions_2.bump("xxx")
			sleep 0.5
			expect(notified).to eq false
			bumped = pg_versions_2.bump("yyy")
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		notified = true
		thread1.pop
		expect(notification.changed).to eq({'yyy'=>bumped['yyy']})
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "can be dropped" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe()
		notified = false
		t1 = Thread.new {
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			sleep 0.5
			expect(notified).to eq false
			subscription.drop
			thread1 << :your_turn
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait
		notified = true
		thread1.pop
		expect(notification).to be_nil
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "can be used with ActiveRecord" do
		ActiveRecord::Base.configurations = { default_env: { adapter: 'postgresql', host: '127.0.0.1', username: 'test', password: 'test', database: 'test', port: 5432 } }
		connection = ActiveRecord::Base.establish_connection

		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process(ActiveRecord::Base) }
		subscription = pg_versions.subscribe("xxx")
		subscription.read(notify: false)
		bumped = false
		t1 = Thread.new {
			bumped = ActiveRecord::Base.bump("xxx")
			thread1 << :your_turn
		}
		notification = subscription.wait
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})

		pg_versions.close
		processing_thread.join
		connection.disconnect
		ActiveRecord::Base.remove_connection
		t1.join
	end

	it "prints something to stderr if it can't connect to db" do
		backup = $stderr
		r, $stderr = IO.pipe
	 	pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process(host: "localhost") }
	 	sleep 0.5
		expect(r.read_nonblock(1000)).to match /failed/
		$stderr = backup
		pg_versions.close
		processing_thread.join
	 end

	it "allows creating subscription with droping-after-use block" do
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		pg_versions.subscribe("xxx") { |subscription|
			expect(subscription).to be_kind_of PgVersions::Connection::Subscription
			#TODO ensure drop was called
		}
		pg_versions.close
		processing_thread.join
	end


	it "allows waiting for version bumps with 'each' loop, exiting on drop" do
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx", batch_delay: nil)
		t1 = Thread.new {
			received_notifications = 0
			subscription.each { |notification|
				received_notifications += 1
			}
			received_notifications
		}

		pg_versions_2 = PgVersions::Connection.new
		processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
		pg_versions_2.bump("xxx")
		sleep 0.1
		pg_versions_2.bump("xxx")
		sleep 0.1
		subscription.drop
		expect(t1.value).to eq 3
		pg_versions.close
		processing_thread.join
		pg_versions_2.close
		processing_thread2.join
		t1.join
	end


	it "allows creating connection with a block for creating pg connection" do

		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { 
			pg_versions.process { |reset, &block| 
				connection = PG.connect(dbname: 'test', user: 'test', password: 'test', host: '127.0.0.1', port: 5432)
				block.call(connection)		
			}
		}

		subscription = pg_versions.subscribe("xxx")
		subscription.wait

		t1 = Thread.new {
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			pg_versions_2.bump("xxx")
			pg_versions_2.close
			processing_thread2.join
		}
		subscription.wait
		pg_versions.close
		processing_thread.join
		t1.join
	end


	it "allows waiting extra time after first event, to collect more events" do
		thread1 = Queue.new
		thread2 = Queue.new
		pg_versions = PgVersions::Connection.new
		processing_thread = Thread.new { pg_versions.process($connection) }
		subscription = pg_versions.subscribe("xxx","yyy", batch_delay: 1.0)
		event = subscription.wait
		expect(event).not_to be_nil
		bumped = {}
		t1 = Thread.new {
			sleep 0.25
			pg_versions_2 = PgVersions::Connection.new
			processing_thread2 = Thread.new { pg_versions_2.process($connection2) }
			bumped.merge!(pg_versions_2.bump("xxx"))
			sleep 0.5
			bumped.merge!(pg_versions_2.bump("yyy"))
			thread2.pop
			sleep 0.25
			bumped.merge!(pg_versions_2.bump("xxx"))
			thread1 << :go
			sleep 0.5
			bumped.merge!(pg_versions_2.bump("yyy"))
			thread1 << :go
			pg_versions_2.close
			processing_thread2.join
		}
		notification = subscription.wait()
		expect(notification.changed).to eq({'xxx'=>bumped['xxx'], 'yyy'=>bumped['yyy']})
		thread2 << :go
		notification = subscription.wait(batch_delay: 0.1)
		thread1.pop
		expect(notification.changed).to eq({'xxx'=>bumped['xxx']})
		notification = subscription.wait(batch_delay: nil)
		thread1.pop
		expect(notification.changed_versions).to eq({'yyy'=>bumped['yyy']})
		pg_versions.close
		processing_thread.join
		t1.join
	end

end