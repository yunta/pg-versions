CREATE UNLOGGED TABLE pg_versions (channel text NOT NULL PRIMARY KEY, instant timestamptz NOT NULL, counter smallint NOT NULL);
