# Operations on versions:
# 1. bump - increase or assert version. ensures new version is unique and higher than any previous version, even if table entry existed before and got removed
# 2. read - always returns a version, even if table entry is missing
# 3. remove (clean) - for periodic pruning of old entries
#
# A "version" (and database schema) contains both: timestamp and a counter (smallint).
#
# We need the timestamp because entries may vanish from the table (either the table is unlogged and a crash occured, or entries were removed for periodic cleanup).
# If an entry has vanished, and bump or read wants to generate a new version, they have to ensure that the new version is higher than any version the key may have had before.
# Using current timestamp of microsecond precision gives near-certainty of having unique first component of the version.
# There remains a theoretical risk that a timestamp was placed in the table, then got deleted (periodic cleanup), and then bump or read generated identical timestamp.
# All those actions would have to happen in the same microsecond, which, having accurate time source, is likely impossible with postgres (yet!).
# To eliminate the risk, delete operation on the table should be followed by at least one microsecond of sleep (or longer if time source is of lower precission), while holding an exclusive lock on the table.
# Bumps and reads always acquire shared lock on the table before reading the timestamp, to ensure that no delete operation gets executed between timestamp generation and version bump.
# Also, if using unlogged table, administrator should ensure that new database instance will not restart, recover, and resume servicing requests faster than one microsecond after the crash XD
#
# Besides the timestamp, we also need the counter. Counter helps solving two scenarios:
# 1) Hypothetical: Two transactions read out the timestamp at the same microsecond. Without the counter the versions asserted by both of those transactions would be identical.
# 2) Practical: Timestamp read-out and row-update are separate operations. It's possible that entire other bump fits between them, leading to assertion of lower version by the transaction which started first and ended last.
# With the counter, bump operation can detect that its timestamp is older than the one already present in the row, and bump the counter instead, leaving newer timestamp in place.
# TODO: think if early row lock wouldn't be better
#
# Read of non-existent row should trigger a bump and reread. Otherwise following scenario may happen:
# Empty table. Bump starts, gets timestamp, loses cpu. Reader reads, gets nothing, reports (current) timestamp higher than the one acquired by bump. Bump commits. Subsequent read reports older timestamp than previous read (loss of monotonicity).
#


require 'set'
require 'pg'

#TODO: prepared statements?
#TODO: use ractor instead of thread for event listening

module PgVersions


	class InvalidParameters < StandardError; end
	class ConnectionClosed < StandardError; end

	def self.timestamp_to_integers(input)
		"to_char(%s, 'YYYYMMDD')::integer || ',' || to_char(%s, 'HH24MISS')::integer || ',' || to_char(%s, 'US')::integer"%[input, input, input]
	end


	def self.with_connection(pg_connection_param, reset, &block)
		if pg_connection_param.kind_of? ::PG::Connection
			if reset
				pg_connection_param.sync_reset
				pg_connection_param.exec("select;")
			end
			block.call(pg_connection_param)
		elsif pg_connection_param.respond_to? :call
			pg_connection_param.call(reset, &block)
		elsif pg_connection_param.kind_of?(String) or pg_connection_param.kind_of?(Hash)
			Thread.handle_interrupt(Object => :never) {
				begin
					pg_connection = nil
					Thread.handle_interrupt(Object => :immediate) {
						pg_connection = ::PG.connect(pg_connection_param)
						block.call(pg_connection)
					}
				ensure
					pg_connection&.close
				end
			}
		elsif defined?(ActiveRecord) and pg_connection_param.kind_of?(Class) and pg_connection_param <= ActiveRecord::Base
			pg_connection = pg_connection_param.connection.raw_connection
			if reset
				pg_connection.sync_reset
				pg_connection.exec("select;")
			end
			block.call(pg_connection)
		else
			raise InvalidParameters, "Invalid connection parameter (#{pg_connection_param.inspect}). Either pass PG::Connection object, url string, hash, ActiveRecord::Base class (or subclass) or call one of the ActiveRecord methods that come with PgVersions refinement."
		end
	end


	refine ActiveRecord::Base.singleton_class do
		def bump(*channels)
			PgVersions::bump(self, *channels)
		end

		def read(*channels)
			PgVersions::read(self, *channels)
		end
	end


	def self.string_to_version(version_str)
		version_str.split(",").map { |str| Integer(str) }
	end


	def self.create_table(connection)
		PgVersions.with_connection(connection, false) { |pg_connection|
			open(File.dirname(__FILE__)+"/../../create-table.sql") { |sql_file|
				pg_connection.exec sql_file.read
			}
		}
	end


	def self.drop_table(connection)
		PgVersions.with_connection(connection, false) { |pg_connection|
			open(File.dirname(__FILE__)+"/../../drop-table.sql") { |sql_file|
				pg_connection.exec sql_file.read
			}
		}
	end


	def self.bump_sql(*channels)
		channels = [channels].flatten.sort
		return ""  if channels.size == 0
		encoder = PG::TextEncoder::QuotedLiteral.new(elements_type: PG::TextEncoder::String.new)
		quoted_channels = channels.map.with_index { |channel, i| "(#{i},#{encoder.encode(channel)})" }.join(", ")
		# table-wide share lock is there to mutually exclude table cleaner
		# clock_timestamp() - this has to be a timestamp after table lock got acquired
		"
			LOCK TABLE pg_versions IN ACCESS SHARE MODE;
			WITH
				to_bump(i, channel) AS (VALUES #{quoted_channels})
				, current_instant(ts) AS (VALUES (clock_timestamp()))
				, updated AS (
						INSERT INTO pg_versions(channel, instant, counter)
						SELECT to_bump.channel, (SELECT ts FROM current_instant), 0 FROM to_bump
						ON CONFLICT (channel) DO UPDATE SET
							instant = GREATEST(pg_versions.instant, EXCLUDED.instant),
							counter = CASE WHEN pg_versions.instant < EXCLUDED.instant THEN 0 ELSE pg_versions.counter + 1 END
						RETURNING channel, instant, pg_versions.counter
						)
			SELECT DISTINCT
				i
				, #{timestamp_to_integers('updated.instant')} || ',' || updated.counter::text AS version
				, pg_notify(updated.channel::text, #{timestamp_to_integers('updated.instant')} || ',' || updated.counter::text)::text
			FROM
				to_bump
				JOIN updated ON to_bump.channel = updated.channel;
		"
	end


	#TODO: ensure this is called only once per transaction, or that all bumps occur in the same order in all transactions, to avoid deadlocks
	def self.bump(connection, *channels)
		channels = [channels].flatten.sort
		PgVersions.with_connection(connection, false) { |pg_connection|
			sql = self.bump_sql(*channels)
			return {} if sql == ""
			pg_connection.exec(sql) { |result|
				result.map { |row| [channels[Integer(row["i"])], string_to_version(row["version"])] }.to_h
			}
		}
	end

	
	#TODO: bump in the same query instead of calling bump
	#TODO: do we really need to bump though?
	#TODO: and then, implement read_sql
	def self.read(connection, *channels)
		PgVersions.with_connection(connection, false) { |pg_connection|
			channels = [channels].flatten.sort
			return {} if channels.size == 0
			versions = {}
			quoted_channels = channels.map.with_index { |channel, i| "(#{i},'#{pg_connection.escape_string(channel)}')" }.join(", ")
			not_found_channels = pg_connection.exec("
				LOCK TABLE pg_versions IN ACCESS SHARE MODE;
				WITH
					channels(i, channel) AS (VALUES #{quoted_channels})
				SELECT
					i
					, #{timestamp_to_integers('instant')} || ',' || counter AS version
				FROM
					channels
					JOIN pg_versions ON pg_versions.channel = channels.channel
				ORDER BY
					i DESC;
			") { |result|
				result.each { |row|
					versions[channels.delete_at(Integer(row["i"]))] = string_to_version(row["version"])
				}
			}
			versions.merge!(self.bump(pg_connection, channels))  if channels.size > 0
			versions
		}
	end


	class Notification
		attr_reader :changed_versions, :changed, :all_versions, :versions
		def initialize(changed_versions, all_versions)
			@changed_versions, @all_versions = changed_versions, all_versions
			@changed, @versions = changed_versions, all_versions
		end
	end


	class ConnectionInner
		
		def initialize()
			@mutex = Mutex.new
			@command_notify_w = nil
			@subscriptions = {}
			@bumps = []
			@reads = []
			@closers = []
			@state = :idle # idle, processing, closing, closed
		end


		def process
			Thread.handle_interrupt(Object => :never) do
				command_notify_r = nil
				@mutex.synchronize {
					case @state
					when :idle
						@state = :processing
					when :processing
						raise "Attempt to run processing on a connection that is already being processed"
					when :closing, :closed
						return
					end
				}
				begin
					command_notify_r, @command_notify_w = IO.pipe					
					Thread.handle_interrupt(Object => :immediate) {
						yield command_notify_r
					}
				ensure
					@mutex.synchronize {
						command_notify_r&.close
						@command_notify_w&.close
						@command_notify_w = nil
						case @state
						when :idle, :closed
							raise "'processor exit in #{@state} state. Please inform the developer of this gem."
						when :processing
							@state = :idle
						when :closing
							@state = :closed
							@closers.each { |closer|
								closer.push true
							}
						end
					}
				end
			end
		end

		
		def wake_processor
			@command_notify_w&.write('!')
			@command_notify_w&.flush
		end


		def get_channels
			@mutex.synchronize {
				return @subscriptions.keys
			}
		end


		def notify(channel, version)
			@mutex.synchronize {
				(@subscriptions[channel] or []).each { |subscriber|
					subscriber.notify({ channel => version })
				}
			}
		end


		def taking_bumps
			@mutex.synchronize {
				yield @bumps
				@bumps = []
			}
		end


		def taking_reads
			@mutex.synchronize {
				yield @reads
				@reads = []
			}
		end


		def bump(channels)
			result = Queue.new
			@mutex.synchronize {
				raise ConnectionClosed  if @state == :closing || @state == :closed
				@bumps << [result, channels]
			}
			wake_processor
			result.pop
		end


		def bump_nonblock(channels)
			@mutex.synchronize {
				raise ConnectionClosed  if @state == :closing || @state == :closed
				@bumps << [nil, channels]
			}
			wake_processor
			nil
		end


		def read(channels)
			result = Queue.new
			@mutex.synchronize {
				raise ConnectionClosed  if @state == :closing || @state == :closed
				@reads << [result, channels]
			}
			wake_processor
			result.pop
		end


		def subscribe(subscriber, channels)
			@mutex.synchronize {
				raise ConnectionClosed  if @state == :closing || @state == :closed
				channels.each { |channel|
					@subscriptions[channel] = []  if @subscriptions[channel].nil?
					@subscriptions[channel].push(subscriber)
				}
			}
			subscriber.notify(read(channels))  # this runs wake_processor, so not doing it explicitly
			true
		end


		def unsubscribe(subscriber, channels)
			@mutex.synchronize {
				raise ConnectionClosed  if @state == :closing || @state == :closed
				channels.each { |channel|
					@subscriptions[channel].delete(subscriber)
					@subscriptions.delete(channel)  if @subscriptions[channel].size == 0
				}
			}
			wake_processor
			true
		end


		def is_closing
			@mutex.synchronize {
				return @state == :closing
			}
		end


		def close
			result = Queue.new
			@mutex.synchronize {
				case @state
				when :idle
					@state = :closed
					return
				when :processing 
					@state = :closing
					@closers << result
					wake_processor
				when :closing
					@closers << result
				when :closed
					return
				end
			}
			result.pop
		end

	end


	class Connection

		def initialize()
			@inner = ConnectionInner.new
		end


		def process(connection_param=nil, autoreconnect=true, &block)
			raise "Both 'connection_param' and a block were given. Don't know which to use."  if !connection_param.nil? and !block.nil?
			connection_param ||= block

			retry_on_exceptions = [ ::PG::ConnectionBad, ::PG::UnableToSend ]
			retry_delay = 0
			
			@inner.process do |notification_r|
				raise if not notification_r
				PgVersions.with_connection(connection_param, true) do |pg_connection|

					listening_to_channels = @inner.get_channels
					listening_to_channels.each { |channel|
						pg_connection.exec("LISTEN #{::PG::Connection.quote_ident(channel)}")
					}
					PgVersions.read(pg_connection, listening_to_channels).each { |channel, version|
						@inner.notify(channel, version)
					}

					loop {
						channels_to_listen_to = @inner.get_channels
						(listening_to_channels - channels_to_listen_to).each { |removed_channel| 
							pg_connection.exec("UNLISTEN #{::PG::Connection.quote_ident(removed_channel)}")
						}
						(channels_to_listen_to - listening_to_channels).each { |added_channel| 
							pg_connection.exec("LISTEN #{::PG::Connection.quote_ident(added_channel)}")
						}
						listening_to_channels = channels_to_listen_to

						@inner.taking_bumps { |bumps|
							channels_to_bump = bumps.map(&:last).flatten.uniq
							bumped_versions = PgVersions.bump(pg_connection, channels_to_bump)
							bumps.each { |bumper, channels|
								bumper.push bumped_versions.slice(*channels)  if not bumper.nil?
							}
						}

						@inner.taking_reads { |reads|
							channels_to_read = reads.map(&:last).uniq
							read_versions = PgVersions.read(pg_connection, channels_to_read)
							reads.each { |reader, channels|
								reader.push read_versions.slice(*channels)
							}
						}

						break if @inner.is_closing

						while notification = pg_connection.notifies
							channel, payload = notification[:relname], notification[:extra]
							@inner.notify(channel, PgVersions.string_to_version(payload))
						end

						#TODO: handle errors
						reads,_writes,_errors = IO::select([pg_connection.socket_io, notification_r])
						pg_connection.consume_input  if reads.include?(pg_connection.socket_io)
						notification_r.read(1)  if reads.include?(notification_r)  #TODO: read everything that can be read here

					}
				end
			rescue *retry_on_exceptions => error
				raise  if connection_param.kind_of?(::PG::Connection)  or  !autoreconnect
				return if @inner.is_closing
				$stderr.puts "Pg connection failed (retrying in #{retry_delay/1000.0}s):\n\t#{error.message}"
				sleep retry_delay/1000.0
				retry_delay = { 0=>100, 100=>1000, 1000=>2000, 2000=>2000 }[retry_delay]
				retry
			end
		end


		def close
			@inner.close
		end


		def bump(*channels)
			@inner.bump(channels)
		end


		def read(*channels)
			@inner.read(channels)
		end


		def subscribe(*channels, known: {}, batch_delay: 0.01)
			subscription = Subscription.new(@inner, batch_delay)
			subscription.subscribe([channels].flatten, known: known)
			if block_given?
				Thread.handle_interrupt(Object => :never) {
					begin
						Thread.handle_interrupt(Object => :immediate) {
							yield subscription
						}
					ensure
						subscription.drop
					end
				}
			else
				subscription
			end
		end


		alias_method :each, def for_each_notification(*channels, known: {}, batch_delay: 0.01, &block)
			subscribe(*channels, known: known, batch_delay: batch_delay) { |subscription|
				subscription.for_each_notification(&block)
			}
		end


		class Subscription

			def initialize(inner, batch_delay)
				@inner = inner
				@batch_delay = batch_delay
				@notifications = Queue.new
				@already_known_versions = Hash.new { |h,k| h[k] = [] }
				@channels = Hash.new(0)
			end


			def subscribe(channels, known: {})
				update_already_known_versions(known)
				channels = [channels].flatten
				channels.select! { |channel|
					(@channels[channel] += 1) == 1
				}
				if channels.size > 0
					@inner.subscribe(self, channels)
				end
			end


			def unsubscribe(*channels)
				channels = [channels].flatten
				channels.select! { |channel|
					@channels[channel] -= 1
					raise "Trying to unsubscribe from channel (%p) more times than it was subscribed to"%[channel]  if @channels[channel] < 0
					@channels.delete(channel)  if @channels[channel] == 0
					not @channels.has_key?(channel)
				}
				@inner.unsubscribe(self, channels)
			end


			def read(*channels, notify: false)
				channels = @channels.keys  if channels.size == 0
				versions = @inner.read(channels)
				update_already_known_versions(versions)  if not notify
				versions
			end


			def bump(*channels, notify: false)
				channels = @channels.keys  if channels.size == 0
				versions = @inner.bump(channels)
				update_already_known_versions(versions)  if not notify
				versions
			end


			#TODO: make this resume-able after forced exception
			def wait(new_already_known_versions = {}, batch_delay: nil)
				batch_delay = @batch_delay  if batch_delay.nil?
				update_already_known_versions(new_already_known_versions)
				loop {
					events = [@notifications.shift]
					sleep batch_delay  if batch_delay
					events << @notifications.shift  while not @notifications.empty?
					changed_versions = {}
					events.each { |versions|
						return nil  if not versions #termination
						versions.each { |channel, version|
							if (@already_known_versions[channel] <=> version) == -1
								@already_known_versions[channel] = version
								changed_versions[channel] = version
							end
						}
					}
					if changed_versions.size > 0
						return Notification.new(changed_versions, @already_known_versions.dup)
					end
				}
			end

			alias_method :each, def for_each_notification(new_already_known_versions = {}, batch_delay: nil)
				update_already_known_versions(new_already_known_versions)
				while notification = wait(batch_delay: batch_delay)
					yield notification
				end
			end


			def notify(versions)
				@notifications << versions
			end


			def drop
				@notifications << nil
				@inner.unsubscribe(self, @channels.keys)  if @channels.keys.size > 0
				#TODO: what to do if this object gets used after drop?
			end


			def update_already_known_versions(new_already_known_versions)
				new_already_known_versions.each { |channel, version|
					@already_known_versions[channel] = version  if (version <=> @already_known_versions[channel]) == 1
				}
			end

		end

	end

end
