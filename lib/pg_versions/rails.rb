if defined? ::Rails
	module PgVersions
		class Engine < ::Rails::Engine
			isolate_namespace PgVersions
		end
	end
end
